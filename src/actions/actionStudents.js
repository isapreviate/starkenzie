export const students = (students) => ({
    type: "NEW_STUDENTS",
    students
  });

export const answers = (answer) => ({
  type: "ANSWERS",
  answer
});