import axios from "axios";

const courseJavascript = (listJavascript) => ({
  type: "GET_LIST_JAVASCRIPT",
  listJavascript,
});

export const getCoursesJavascript = (token) => (dispatch) => {
  axios
    .get(`https://estevao-api.herokuapp.com/contents`, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      const javascript = res.data.filter((item) => item.js);
      dispatch(courseJavascript(javascript));
    });
};
