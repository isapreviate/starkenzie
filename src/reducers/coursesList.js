const defaultState = {
  html: [],
  css: [],
  javascript: [],
  modeEdit: "",
  modeEditData: [],
};

const coursesList = (state = defaultState, action) => {
  switch (action.type) {
    case "GET_LIST_HTML":
      // const { listHTML } = action;
      return {
        ...state,
        html: [...action.listHTML],
      };

    case "GET_LIST_CSS":
      // const { listHTML } = action;
      return {
        ...state,
        css: [...action.listCSS],
      };

    case "GET_LIST_JAVASCRIPT":
      // const { listHTML } = action;
      return {
        ...state,
        javascript: [...action.listJavascript],
      };

    case "DEL_MODULE_HTML":
      state.html.splice(action.key, 1);
      return {
        ...state,
        html: [...state.html],
      };

    case "DEL_MODULE_CSS":
      state.css.splice(action.key, 1);
      return {
        ...state,
        css: [...state.css],
      };

    case "DEL_MODULE_JAVASCRIPT":
      state.javascript.splice(action.key, 1);
      return {
        ...state,
        javascript: [...state.javascript],
      };

    case "MODE_EDIT":
      return {
        ...state,
        modeEdit: action.mode,
      };

    case "MODE_EDIT_DATA":
      return {
        ...state,
        modeEditData: action.data,
      };

    default:
      return state;
  }
};

export default coursesList;
