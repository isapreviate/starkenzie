const defaultState = { currentToken: null, user: {} };

const login = (state = defaultState, action) => {
  switch (action.type) {
    case "LOGIN":
      const { currentToken, user } = action;

      return { ...state, currentToken, user };

    case "LOGOUT":
      return defaultState;

    default:
      return state;
  }
};

export default login;
