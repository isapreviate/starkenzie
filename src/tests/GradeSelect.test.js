import React from "react";
import renderer from "react-test-renderer";
import GradeSelect from "../components/userform/GradeSelect";

test("", () => {
  const component = renderer.create(<GradeSelect />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
