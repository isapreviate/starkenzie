const errorRules = {
  name: [
    {
      validator: (rule, value) => {
        if (/[^áéía-zA-Z]+/gi.test(value)) {
          return Promise.reject("Somente letras");
        }
        return Promise.resolve();
      },
    },
    { required: true, message: "Campo obrigatório" },
  ],

  user: [
    {
      validator: (rule, value) => {
        if (/[^a-zA-Z0-9]+/gi.test(value)) {
          return Promise.reject("Somente letras e números");
        }
        return Promise.resolve();
      },
    },

    { required: true, message: "Campo obrigatório" },
  ],

  email: [
    {
      type: "email",
      message: "Email inválido.",
    },
    {
      required: true,
      message: "Campo obrigatório",
    },
  ],

  cpf: [
    {
      validator: (rule, value) => {
        if (!value) {
          return Promise.reject("Campo obrigatório");
        }
        if (value.replace(/\D+/g, "").length < 11) {
          return Promise.reject("CPF incompleto");
        }
        return Promise.resolve();
      },
    },
  ],
  phone: [
    {
      validator: (rule, value) => {
        if (!value) {
          return Promise.reject("Campo obrigatório");
        }
        if (value.replace(/\D+/g, "").length < 11) {
          return Promise.reject("Telefone incompleto");
        }
        return Promise.resolve();
      },
    },
  ],

  address: [
    {
      required: true,
      message: "Campo obrigatório",
    },
  ],

  birth: [
    {
      required: true,
      message: "Campo obrigatório",
    },
  ],

  grade: [
    {
      required: true,
      message: "Campo obrigatório",
    },
  ],

  password: [
    {
      required: true,
      message: "Campo obrigatório",
    },
  ],
};

export default errorRules;
