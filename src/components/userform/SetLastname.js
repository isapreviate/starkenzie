import React from "react";
import { Input } from "antd";
import { UserOutlined } from "@ant-design/icons";

const style = {
  width: "280px",
  height: "50px",
  borderRadius: "4px",
  outline: "none",
  fontSize: "15px",
  boxSizing: "border-box",
};

const SetLastname = () => (
  <Input
    style={{ ...style }}
    placeholder="Sobrenome"
    suffix={<UserOutlined />}
  />
);

export default SetLastname;
