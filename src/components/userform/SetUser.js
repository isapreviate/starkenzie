import React from "react";
import { Input } from "antd";
import { UserAddOutlined } from "@ant-design/icons";

const style = {
  width: "280px",
  height: "50px",
  borderRadius: "4px",
  outline: "none",
  fontSize: "15px",
  boxSizing: "border-box",
};

const SetUser = () => (
  <Input
    suffix={<UserAddOutlined />}
    style={{ ...style }}
    placeholder="Usuário"
  />
);

export default SetUser;
