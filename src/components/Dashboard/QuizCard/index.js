import React, { useEffect, useState } from "react";
import { Card, Radio } from 'antd';
import { useSelector } from "react-redux";
import axios from "axios";
import { message } from "antd";




const QuizCard = () => {

  const userToken = useSelector((state) => state.session.currentToken);
  const userId = useSelector((state) => state.session.user.id);
  const [data, setData] = useState("");

  const getQuestions = () => {
    axios
      .get("https://estevao-api.herokuapp.com/questions", {
        headers: {
          Authorization: userToken,
        },
      })
      .then(
        (res) => {
          let data = objectConstructor(res.data)
          setData(data)
        },
        (error) => message.error("Houve algum erro ao buscar os alunos.", 1.5)
      );
  };

  const objectConstructor = (data) => {
    let res = {}
    if (!data) {
      return res
    }

    return data
      .filter(question => question.question !== null)
      .map(question => {
        let res = {}
        res["question"] = question.question
        res["answer"] = JSON.parse(question.answer)
        res["id"] = question.id
        return res
      })
  }

  useEffect(() => {
    getQuestions();
  }, []);

  const setAnswers = (questionId, answer) => {

    axios.post(
      `https://estevao-api.herokuapp.com/users/${userId}/answer_stundents`,
      {
        answer_stundent: {
          answer: answer,
          question_id: questionId
        }
      },
      {
        headers: {
          Authorization: userToken,
        },
      }
    )
      .then(
        (res) => {
          if (res.status === 201) {
            message.success("Resposta Enviada!", 1.5);
          }
        },
        (error) =>
          message.error("Houve algum erro ao cadastrar essa resposta.", 1.5)
      );

  }

  return (

    <>
      <div style={{ width: "700px", margin: "24px", padding: "20px", backgroundColor: "#F2F2F2", display: "flex", flexDirection: "row", flexWrap: "wrap", justifyContent: "flex-start", alignItems: "flex-start" }}>
        {!data ? <div></div> : (

          data.map(question =>

            <Card style={{ width: 300, height: "200px", backgroundColor: "#DADBDC", margin: "10px", display: "flex", flexDirection: "row", justifyContent: "center", alignItems: "flex-start" }} key={question.id} bordered={true} >
              <p>{question.question}</p>
              <Radio.Group onChange={(e) => setAnswers(question.id, e.target.value)}>
                <p><Radio value={question.answer.answer1}>{question.answer.answer1}</Radio></p>
                <p><Radio value={question.answer.answer2}>{question.answer.answer2}</Radio></p>
                <p><Radio value={question.answer.answer3}>{question.answer.answer3}</Radio></p>
                <p><Radio value={question.answer.answer4}>{question.answer.answer4}</Radio></p>
              </Radio.Group>
            </Card >

          ))}
      </div>
    </>
  )
}

export default QuizCard;
