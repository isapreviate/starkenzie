import React, { useState } from "react";
import TextArea from "antd/lib/input/TextArea";
import { Form, Select, Layout, Button, Input, message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { setModeEdit } from "../../../actions/setModeEdit";
const { Item } = Form;
const { Option } = Select;
const { Header, Content } = Layout;

const style = {
  height: "40px",
  borderRadius: "4px",
  outline: "none",
  fontSize: "15px",
  boxSizing: "border-box",
};

const ContentModuleEdit = () => {
  const listCourses = useSelector(({ coursesList }) => coursesList);
  const userToken = useSelector((state) => state.session.currentToken);
  const [title, setTitle] = useState(listCourses.modeEditData.title);
  const [content, setContent] = useState(listCourses.modeEditData.content);
  const dispatch = useDispatch();
  const [initialMater, setInitMater] = useState(listCourses.modeEditData.mater);
  const id = listCourses.modeEditData.id;

  const onFinish = (title, content, initialMater) => {
    const data = {
      title: title,
      content: content,
    };
    axios
      .put(
        `https://estevao-api.herokuapp.com/contents/${id}`,
        {
          content: {
            [initialMater]: JSON.stringify(data),
          },
        },
        {
          headers: {
            Authorization: userToken,
          },
        }
      )
      .then(
        (res) => {
          if (res.status === 200) {
            message.success("Conteúdo Alterado!", 1.5);
            dispatch(setModeEdit(false));
          }
        },
        (error) => message.error("Houve algum erro ao cadastrar conteúdo.", 1.5)
      );
  };

  return (
    <Layout
      className="site-layout"
      style={{ width: "430px", minHeight: "80vh" }}
    >
      <Header
        className="site-layout-background"
        style={{ background: "rgba(255, 255, 255, 0.2)", padding: 0 }}
      />
      <Content style={{ margin: "0 16px" }}>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 360 }}
        >
          <Form onFinish={() => onFinish(title, content, initialMater)}>
            <Item>
              <Select
                defaultValue={initialMater}
                placeholder="Matéria"
                onChange={setInitMater}
              >
                <Option value="html">HTML</Option>
                <Option value="css">CSS</Option>
                <Option value="js">JS</Option>
              </Select>
            </Item>
            <Item>
              <Input
                style={{ ...style, width: "100%" }}
                placeholder="Título"
                onChange={(e) => setTitle(e.target.value)}
                value={title}
              />
            </Item>
            <Item>
              <TextArea
                rows={10}
                onChange={(e) => setContent(e.target.value)}
                value={content}
              />
            </Item>

            <Button type="primary" htmlType="submit">
              Gravar Conteúdo
            </Button>
          </Form>
        </div>
      </Content>
    </Layout>
  );
};

export default ContentModuleEdit;
