import React from "react";
import { Button } from "antd";
import { EditOutlined } from "@ant-design/icons";

const ButtonEdit = ({ onClick }) => (
  <Button
    onClick={onClick}
    style={{
      width: "100px",
      marginRight: "6px",
      marginTop: "6px",
      borderRadius: "6px",
    }}
    type="primary"
  >
    <EditOutlined /> Editar
  </Button>
);

export default ButtonEdit;
