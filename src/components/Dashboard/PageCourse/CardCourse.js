import React, { useState } from "react";
import styled from "styled-components";
import { DownOutlined, UpOutlined } from "@ant-design/icons";

const CardCourse = ({ title, children, buttonDelete }) => {
  const [toggle, setToggle] = useState(false);
  const showContent = () => setToggle(!toggle);
  return (
    <Container>
      <Header onClick={showContent}>
        <Title>{title}</Title>
        {toggle ? <UpOutlined /> : <DownOutlined />}
      </Header>

      {toggle && <Content>{children}</Content>}
    </Container>
  );
};

export default CardCourse;

const Container = styled.div`
  width: 100%;
  height: auto;
  background: #4192d9;
  margin-bottom: 20px 0px;
  border-radius: 5px;
  cursor: pointer;
  transition-duration: 10s;
  transition-delay: 10s;
`;

const Header = styled.div`
  width: 100%;
  height: 80px;
  padding: 20px;
  background: none;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const Title = styled.h1`
  margin: 0px;
  padding: 0px;
  color: #fff;
  font-size: 20px;
`;

const Content = styled.div`
  width: 90%;
  height: auto;
  padding: 16px;
  background: none;
`;
