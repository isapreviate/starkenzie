import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import ButtonDelete from "./ButtonDelete";
import ButtonEdit from "./ButtonEdit";
import CardCourse from "./CardCourse";
import ContentModuleEdit from "./ContentModuleEdit";
import { Modal } from "antd";
import { setModeEdit, setModeEditData } from "../../../actions/setModeEdit";
import {
  delModuleHtml,
  delModuleCSS,
  delModuleJS,
} from "../../../actions/delModule";
import { getCoursesHTML } from "../../../actions/getCoursesHTML";
import { getCoursesCSS } from "../../../actions/getCoursesCSS";
import { getCoursesJavascript } from "../../../actions/getCoursesJavascript";

import { ExclamationCircleOutlined } from "@ant-design/icons";
import axios from "axios";

const PageCourse = () => {
  const dispatch = useDispatch();
  const listCourses = useSelector(({ coursesList }) => coursesList);
  const [currentUser, currentToken] = useSelector(({ session }) => [
    session.user,
    session.currentToken,
  ]);

  useEffect(() => {
    dispatch(getCoursesHTML(currentToken));
    dispatch(getCoursesCSS(currentToken));
    dispatch(getCoursesJavascript(currentToken));
  }, [dispatch, currentToken]);

  const delModuleApi = (item) => {
    axios.delete(`https://estevao-api.herokuapp.com/contents/${item}`, {
      headers: {
        Authorization: currentToken,
      },
    });
  };

  const confirmDeleteHtml = (key, item) =>
    Modal.confirm({
      title: "Confirmação de exclusão",
      icon: <ExclamationCircleOutlined />,
      content: "Tem certeza de que deseja remover esse módulo?",
      okText: "Sim",
      cancelText: "Cancelar",
      onOk() {
        dispatch(delModuleHtml(key, item));
        delModuleApi(item);
      },
    });

  const confirmDeleteCSS = (key, item) =>
    Modal.confirm({
      title: "Confirmação de exclusão",
      icon: <ExclamationCircleOutlined />,
      content: "Tem certeza de que deseja remover esse módulo?",
      okText: "Sim",
      cancelText: "Cancelar",
      onOk() {
        dispatch(delModuleCSS(key, item));
        delModuleApi(item);
      },
    });

  const confirmDeleteJS = (key, item) =>
    Modal.confirm({
      title: "Confirmação de exclusão",
      icon: <ExclamationCircleOutlined />,
      content: "Tem certeza de que deseja remover esse módulo?",
      okText: "Sim",
      cancelText: "Cancelar",
      onOk() {
        dispatch(delModuleJS(key, item));
        delModuleApi(item);
      },
    });

  const setFunctions = (item, id, mater) => {
    dispatch(setModeEdit(!listCourses.modeEdit));
    dispatch(setModeEditData({ ...item, id, mater }));
  };
  return listCourses.modeEdit ? (
    <ContentModuleEdit />
  ) : (
    <>
      <Container>
        <Title>Módulo HTML</Title>
        {listCourses.html.map((item, key) => (
          <div style={{ margin: "20px 0px 40px 0px" }} key={key}>
            <CardCourse
              title={JSON.parse(item.html).title}
              children={JSON.parse(item.html).content}
            />
            {currentUser.auth > 1 && (
              <div>
                <ButtonEdit
                  onClick={() =>
                    setFunctions(JSON.parse(item.html), item.id, "html")
                  }
                />
                <ButtonDelete onClick={() => confirmDeleteHtml(key, item.id)} />
              </div>
            )}
          </div>
        ))}
      </Container>

      <Container>
        <Title>Módulo CSS</Title>
        {listCourses.css.map((item, key) => (
          <div style={{ margin: "20px 0px 40px 0px" }} key={key}>
            <CardCourse
              title={JSON.parse(item.css).title}
              children={JSON.parse(item.css).content}
            />
            {currentUser.auth > 1 && (
              <div>
                <ButtonEdit
                  onClick={() =>
                    setFunctions(JSON.parse(item.css), item.id, "css")
                  }
                />
                <ButtonDelete onClick={() => confirmDeleteCSS(key, item.id)} />
              </div>
            )}
          </div>
        ))}
      </Container>

      <Container>
        <Title>Módulo Javascript</Title>
        {listCourses.javascript.map((item, key) => (
          <div style={{ margin: "20px 0px 40px 0px" }} key={key}>
            <CardCourse
              title={JSON.parse(item.js).title}
              children={JSON.parse(item.js).content}
            />
            {currentUser.auth > 1 && (
              <div>
                <ButtonEdit
                  onClick={() =>
                    setFunctions(JSON.parse(item.js), item.id, "js")
                  }
                />
                <ButtonDelete onClick={() => confirmDeleteJS(key, item.id)} />
              </div>
            )}
          </div>
        ))}
      </Container>
    </>
  );
};

const Container = styled.div`
  background-color: #f4f4f4;
  padding: 20px;
  margin-bottom: 18px;
  border-radius: 8px;
`;

const Title = styled.h1`
  font-size: 28px;
`;

export default PageCourse;
