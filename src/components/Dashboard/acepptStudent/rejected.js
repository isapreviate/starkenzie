import React, { useEffect } from "react";
import { Layout } from "antd";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { message } from "antd";
import Students from "./students";
import { students } from "../../../actions/actionStudents";
const { Header, Content, Footer } = Layout;

const RejectedStudent = () => {
  const userToken = useSelector((state) => state.session.currentToken);
  const dispatch = useDispatch();

  const getUsers = () => {
    axios
      .get("https://estevao-api.herokuapp.com/users", {
        headers: {
          Authorization: userToken,
        },
      })
      .then(
        (res) => {
          dispatch(students(res.data));
        },
        (error) => message.error("Houve algum erro ao buscar os alunos.", 1.5)
      );
  };

  useEffect(() => {
    getUsers();
  });

  return (
    <Layout className="site-layout" style={{ minHeight: "100vh" }}>
      <Header
        className="site-layout-background"
        style={{ background: "rgba(255, 255, 255, 0.2)", padding: 0 }}
      />
      <Content style={{ margin: "0 16px" }}>
        <Students refresh={getUsers} status={"false"}/>
      </Content>
      <Footer style={{ textAlign: "center" }}>StarKenzie ©2020</Footer>
    </Layout>
  );
};

export default RejectedStudent;
 