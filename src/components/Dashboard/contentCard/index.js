import React, { useState } from "react";
import TextArea from "antd/lib/input/TextArea";
import { Form, Select, Layout, Button, Input } from "antd";
import { useSelector } from "react-redux";
import axios from "axios";
import { message } from "antd";
const { Item } = Form;
const { Option } = Select;
const { Header, Content } = Layout;

const style = {
  height: "40px",
  borderRadius: "4px",
  outline: "none",
  fontSize: "15px",
  boxSizing: "border-box",
};

const ContentCard = () => {
  const title = "";
  const text = "";
  const [mater, setMater] = useState("");
  const author = useSelector((state) => state.session.user.name);
  const userToken = useSelector((state) => state.session.currentToken);

  const onFinish = (values) => {
    axios
      .post(
        "https://estevao-api.herokuapp.com/contents",
        {
          content: {
            [mater]: JSON.stringify(values),
          },
        },
        {
          headers: {
            Authorization: userToken,
          },
        }
      )
      .then(
        (res) => {
          if (res.status === 201) {
            message.success("Conteúdo Cadastrado!", 1.5);
          }
        },
        (error) => message.error("Houve algum erro ao cadastrar conteúdo.", 1.5)
      );
  };

  return (
    <Layout
      className="site-layout"
      style={{ width: "430px", minHeight: "100vh" }}
    >
      <Header
        className="site-layout-background"
        style={{ background: "rgba(255, 255, 255, 0.2)", padding: 0 }}
      />
      <Content style={{ margin: "0 16px" }}>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 360 }}
        >
          <Form onFinish={(fieldValues) => onFinish(fieldValues)}>
            <Item>
              <Select placeholder="Matéria" onChange={setMater}>
                <Option value="html">HTML</Option>
                <Option value="css">CSS</Option>
                <Option value="js">JS</Option>
              </Select>
            </Item>
            <Item name="title" initialValue={title}>
              <Input style={{ ...style, width: "100%" }} placeholder="Título" />
            </Item>
            <Item name="content" initialValue={text}>
              <TextArea rows={10} />
            </Item>
            <Item name="author" initialValue={author} />
            <Button type="primary" htmlType="submit">
              Gravar Conteúdo
            </Button>
          </Form>
        </div>
      </Content>
    </Layout>
  );
};

export default ContentCard;
