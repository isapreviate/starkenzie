import React from "react";
import TextArea from "antd/lib/input/TextArea";
import { Form, Layout, Button, Input } from "antd";
import { useSelector } from "react-redux";
import axios from "axios";
import { message } from "antd";
const { Item } = Form;
const { Header, Content } = Layout;

const style = {
  width: "280px",
  height: "40px",
  borderRadius: "4px",
  outline: "none",
  fontSize: "15px",
  boxSizing: "border-box",
};

const QuestionCard = () => {


  const author = useSelector((state) => state.session.user.name);
  const userToken = useSelector((state) => state.session.currentToken);

  const onFinish = ({ question, answer1, answer2, answer3, answer4, answer_correct }) => {
    const answer = { answer1: answer1, answer2: answer2, answer3: answer3, answer4: answer4 };
    axios
      .post(
        'https://estevao-api.herokuapp.com/questions',
        {
          question: {
            question: question,
            answer: JSON.stringify(answer),
            answer_correct: answer_correct,
          }
        },
        {
          headers: {
            Authorization: userToken,
          },
        }
      )
      .then(
        (res) => {
          if (res.status === 201) {
            message.success("Questão Cadastrada!", 1.5);
          }
        },
        (error) =>
          message.error("Houve algum erro ao cadastrar essa questão.", 1.5)
      );
  };

  return (

    <Layout className="site-layout" style={{ minHeight: "100vh" }}>
      <Header
        className="site-layout-background"
        style={{ background: "rgba(255, 255, 255, 0.2)", padding: 0 }}
      />
      <Content style={{ margin: "0 16px" }}>
        <div
          className="site-layout-background"
          style={{ padding: 24, minHeight: 360 }}
        >
          <Form onFinish={(fieldValues) => onFinish(fieldValues)}>

            <Item name="question" >
              <TextArea rows={10} />
            </Item>
            <Item name="answer1" >
              <Input
                style={{ ...style, width: "580px" }}
                placeholder="Resposta 1"
              />
            </Item>
            <Item name="answer2" >
              <Input
                style={{ ...style, width: "580px" }}
                placeholder="Resposta 2"
              />
            </Item>
            <Item name="answer3" >
              <Input
                style={{ ...style, width: "580px" }}
                placeholder="Resposta 3"
              />
            </Item>
            <Item name="answer4" >
              <Input
                style={{ ...style, width: "580px" }}
                placeholder="Resposta 4"
              />
            </Item>
            <Item name="answer_correct" >
              <Input
                style={{ ...style, width: "580px" }}
                placeholder="Resposta certa"
              />
            </Item>

            <Item name="author" initialValue={author} />
            <Button type="primary" htmlType="submit">
              Gravar Questão
            </Button>
          </Form>
        </div>
      </Content>
    </Layout>
  );
};

export default QuestionCard;
