import React, { useState } from "react";
import "antd/dist/antd.css";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import { Form, Input, Button, Checkbox } from "antd";
import { LockFilled, UserOutlined } from "@ant-design/icons";
import errorRules from "./validator";
import { requestLogin } from "../../actions/actionLogin";
import BckImg from "./assets/bck-image.jpeg";
import { RootState } from "../../reducers/reducersCombine";

const { Item } = Form;
const { Password } = Input;

const style = {
  height: "50px",
  marginTop: "10px",
  borderRadius: "4px",
  outline: "none",
  fontSize: "16px",
};

interface LoginProps {
  username: string;
  password: string;
}

const Login = (props: LoginProps) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const token = useSelector((state: RootState) => state.session.currentToken);
  const aceppted = useSelector(
    (state: RootState) => state.session.user.aceppted
  );

  const onFinish = () => {
    dispatch(requestLogin(email, password));
  };

  if (token && aceppted === "true") {
    return <Redirect to="/dashboard/home" />;
  }

  return (
    <Body>
      <FormContainer>
        <Form style={{ width: "90%" }} name="normal_login" onFinish={onFinish}>
          <Item name="username" rules={errorRules.username}>
            <Input
              style={{ ...style }}
              suffix={<UserOutlined />}
              placeholder="Usuário"
              onChange={(e) => setEmail(e.target.value)}
            />
          </Item>
          <Item name="password" rules={errorRules.password}>
            <Password
              style={{ ...style }}
              type="password"
              placeholder="Senha"
              onChange={(e) => setPassword(e.target.value)}
            />
          </Item>
          <ItemInfos>
            <Checkbox>Lembrar</Checkbox>
            <Link to="/cadastro">
              <b>Esqueceu a senha?</b>
            </Link>
          </ItemInfos>
          <Item>
            <Button
              style={{
                height: "58px",
                marginTop: "12px",
                marginBottom: "8px",
                width: "100%",
                borderRadius: "4px",
                backgroundColor: "#146DFA",
                fontSize: "16px",
              }}
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              <LockFilled /> LOGIN
            </Button>
            <Link to="/cadastro">Registre-se</Link>
          </Item>
        </Form>
      </FormContainer>
      <ImageContainer />
    </Body>
  );
};

const Body = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
`;

const FormContainer = styled.div`
  width: 25%;
  height: 100vh;
  background: #d4dbf5;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const ImageContainer = styled.div`
  width: 75%;
  height: 100%;
  background-image: url(${BckImg});
  background-position: center;
  background-size: cover;
`;

const ItemInfos = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: 6px 0px;
`;

export default Login;
