const errorRules = {
  username: [
    {
      required: true,
      message: "Campo obrigatório",
    },
  ],

  password: [
    {
      required: true,
      message: "Campo obrigatório",
    },
  ],
};

export default errorRules;
