import React from "react";
import { Route, Switch } from "react-router-dom";
import DashBoard from "./components/Dashboard";
import UserEditForm from "./components/userEditForm/";
import PageCourse from "./components/Dashboard/PageCourse";
import PageTeacher from "./components/Dashboard/PageTeacher";
import ContentCard from "./components/Dashboard/contentCard";
import QuestionCard from "./components/Dashboard/QuestionCard/";
import WaitingStudent from "./components/Dashboard/acepptStudent/waiting";
import RejectedStudent from "./components/Dashboard/acepptStudent/rejected";
import AceptedStudent from "./components/Dashboard/acepptStudent/aceppted";
import QuizCard from "./components/Dashboard/QuizCard";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/dashboard/home">
        <DashBoard />
      </Route>
      <Route exact path="/dashboard/quiz">
        <QuizCard />
      </Route>
      <Route exact path="/dashboard/settings">
        <UserEditForm />
      </Route>
      <Route exact path="/dashboard/course">
        <PageCourse />
      </Route>
      <Route exact path="/dashboard/teachers">
        <PageTeacher />
      </Route>
      <Route exact path="/dashboard/content">
        <ContentCard />
        <QuestionCard />
      </Route>
      <Route exact path="/dashboard/waiting">
        <WaitingStudent />
      </Route>
      <Route exact path="/dashboard/rejected">
        <RejectedStudent />
      </Route>
      <Route exact path="/dashboard/aceppted">
        <AceptedStudent />
      </Route>
    </Switch>
  );
};

export default Routes;
